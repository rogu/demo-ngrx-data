import { EntityMetadataMap, EntityDataModuleConfig, DefaultDataServiceConfig } from '@ngrx/data';

export const defaultDataServiceConfig: DefaultDataServiceConfig = {
  root: 'https://jsonplaceholder.typicode.com',
  timeout: 3000
};

const entityMetadata: EntityMetadataMap = {
  Users: {
    entityName: 'User'
  },
  Posts: {
    entityName: 'Post'
  },
  Core: {
    entityName: 'Core'
  }
};

export const entityConfig: EntityDataModuleConfig = {
  entityMetadata
};
