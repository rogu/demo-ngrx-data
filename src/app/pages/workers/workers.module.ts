import { WorkerModel } from './worker.model';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkersComponent } from './components/workers.component';
import { EntityDefinitionService, EntityMetadata } from '@ngrx/data';

@NgModule({
  declarations: [WorkersComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: WorkersComponent }
    ])
  ]
})
export class WorkersModule {
  constructor(
    entityDefinitionService: EntityDefinitionService
  ) {
    const entityMetadata: EntityMetadata<WorkerModel> = {
      entityName: 'Worker'
    }
    entityDefinitionService.registerMetadata(entityMetadata);
  }
}
