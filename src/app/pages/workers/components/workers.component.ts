import { Observable } from 'rxjs';
import { WorkerModel } from './../worker.model';
import { WorkerService } from '../worker.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-workers',
  template: `
    <div class="space-y-4">
      <dl class="info">
        <dt>
          W przykładzie użyłem
        </dt>
        <dd>Dynamiczne dodanie entity w module workers</dd>
        <dd>Przeciążenie metod getAll oraz delete w serwisie</dd>
      </dl>
      <ul>
        <li *ngFor="let item of data$|async">
          {{item.name}}
          <button (click)='remove(item.id)'>remove</button>
        </li>
      </ul>
    </div>
  `
})
export class WorkersComponent implements OnInit {

  data$: Observable<WorkerModel[]> = this.workerService.entities$;

  constructor(
    private workerService: WorkerService
  ) { }

  ngOnInit(): void {
    this.workerService.getAll().subscribe();
  }

  remove(id) {
    this.workerService.delete(id).subscribe();
  }

}
