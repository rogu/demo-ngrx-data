export interface WorkerModel {
    id?: string,
    name: string,
    phone: number,
    category: string
}
