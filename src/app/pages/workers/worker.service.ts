import { WorkerModel } from './worker.model';
import { Injectable } from '@angular/core';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";
import { Api } from 'src/app/common/api';

@Injectable({ providedIn: 'root' })
export class WorkerService extends EntityCollectionServiceBase<WorkerModel> {
  constructor(
    private http: HttpClient,
    serviceElementsFactory: EntityCollectionServiceElementsFactory
  ) {
    super('Worker', serviceElementsFactory);
  }

  override getAll() {
    this.setLoading(true);
    return this.http.get(Api.DATA_WORKERS).pipe(
      map(({ data }: { data: WorkerModel[] }) => {
        this.setLoading(false);
        this.addAllToCache(data);
        return data;
      })
    );
  }

  override delete(id) {
    this.setLoading(true);
    return this.http.delete(Api.DATA_WORKERS + '/' + id).pipe(
      map(({ data }: any) => {
        this.setLoading(false);
        this.removeOneFromCache(id);
        return data;
      })
    );
  }
}
