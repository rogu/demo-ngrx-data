import { ItemsService } from './../items.service';
import { ItemModel } from './../item.model';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-items',
  template: `
    <div class="space-y-4">
      <dl class="info">
        <dt>
          W przykładzie użyłem
        </dt>
        <dd>Dynamiczne dodanie entity w module items</dd>
        <dd>Custom Data Service - dodatkowa konfiguracja serwisu oraz przeciążanie metod</dd>
        <dd>Strategie pesymistyczną podczas usuwania produktu</dd>
        <dd>Funkcje filtrującą dane</dd>
      </dl>
      <div class="flex gap-4">

          <div >
            <input class="p-2 rounded" placeholder="find by name" type="text" #x (input)='findByName(x.value)'>
          </div>
          <div >
            <div class="bg-blue-100 p-2 mb-2 rounded">
              {{(data$|async|slice:0:10|keyvalue).length}} from {{count$|async}}
            </div>
            <table>
              <thead>
                <tr>
                  <th>TITLE</th>
                  <th>PRICE</th>
                  <th colspan="2">ACTIONS</th>
                </tr>
              </thead>
              <tbody>
                <tr *ngFor="let item of data$|async|slice:0:10">
                        <td>{{item.title}}</td>
                        <td>{{item.price}}</td>
                        <td><button class="" [routerLink]="[item.id]">more</button></td>
                        <td><button (click)="remove(item.id)">remove</button></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

    </div>
  `
})
export class ItemsComponent {

  data$: Observable<ItemModel[]> = this.itemsService.filteredEntities$;
  count$: Observable<number> = this.itemsService.count$;

  constructor(
    private itemsService: ItemsService
  ) { }

  remove(id) {
    this.itemsService.delete(id);
  }

  findByName(name) {
    this.itemsService.setFilter(name);
  }

}
