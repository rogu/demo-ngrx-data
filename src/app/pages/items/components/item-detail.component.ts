import { ItemsService } from './../items.service';
import { ItemModel } from './../item.model';
import { map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Component } from '@angular/core';
import { combineLatest, Observable } from 'rxjs';
import { selectRouterId } from '../../../common/store/core.selectors';

@Component({
    template: `
        <pre>{{data$|async|json}}</pre>
        <a href="javascript:history.back()">back</a>
    `
})

export class ItemDetailComponent {

    data$: Observable<ItemModel> = combineLatest([
        this.store.select(selectRouterId),
        this.itemsService.entityMap$
    ]).pipe(map(([id, entities]) => entities[id]));

    constructor(private store: Store<any>, private itemsService: ItemsService) { }

}
