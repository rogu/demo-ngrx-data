import { ItemsGuard } from './items.guard';
import { ItemDetailComponent } from './components/item-detail.component';
import { ItemModel } from './item.model';
import { ItemsDataService } from './items.dataservice';
import { ItemsComponent } from './components/items.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntityDefinitionService, EntityDataService, EntityMetadata } from '@ngrx/data';

@NgModule({
  declarations: [ItemsComponent, ItemDetailComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: ItemsComponent, canActivate: [ItemsGuard] },
      { path: ':id', component: ItemDetailComponent, canActivate: [ItemsGuard] }
    ])
  ]
})
export class ItemsModule {
  constructor(
    entityDataService: EntityDataService,
    entityDefinitionService: EntityDefinitionService,
    itemsDataService: ItemsDataService
  ) {
    const entityMetadata: EntityMetadata<ItemModel> = {
      entityName: 'Item',
      entityDispatcherOptions: { optimisticDelete: false },
      filterFn(entities: ItemModel[], search: string) {
        return entities.filter(e => e.title.indexOf(search) > -1);
      }
    }
    entityDefinitionService.registerMetadata(entityMetadata);
    entityDataService.registerService('Item', itemsDataService);
  }
}
