import { ItemModel } from './item.model';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
    DefaultDataService,
    HttpUrlGenerator,
    DefaultDataServiceConfig
} from '@ngrx/data';
import { Api } from 'src/app/common/api';

export const config: DefaultDataServiceConfig = {
    root: Api.DATA_URL,
    timeout: 3000
};

@Injectable({ providedIn: 'root' })
export class ItemsDataService extends DefaultDataService<ItemModel> {

    constructor(http: HttpClient, httpUrlGenerator: HttpUrlGenerator) {
        super('Item', http, httpUrlGenerator, config);
    }

    getAll(): any {
        return super.getAll().pipe(
            map(({ data }: any) => data)
        );
    }

}
