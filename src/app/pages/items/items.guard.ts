import { ItemsService } from './items.service';
import { mapTo, filter, first, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class ItemsGuard implements CanActivate {

    constructor(private itemsService: ItemsService) { }

    canActivate(): Observable<boolean> {
        return this.itemsService.entities$.pipe(
            tap((val) => !val.length && this.itemsService.getAll()),
            filter((val) => !!val.length),
            first(),
            mapTo(true)
        );
    }
}
