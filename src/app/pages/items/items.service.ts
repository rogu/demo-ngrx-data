import { ItemModel } from './item.model';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ItemsService extends EntityCollectionServiceBase<ItemModel> {
    constructor(
        entityService: EntityCollectionServiceElementsFactory
    ) {
        super('Item', entityService);
    }
}
