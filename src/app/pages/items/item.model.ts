export interface ItemModel {
    category: string;
    imgSrc: string;
    price: number;
    title: string;
    id?: string;
}
