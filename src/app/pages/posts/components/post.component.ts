import { selectRouterId } from '../../../common/store/core.selectors';
import { PostsService } from '../services/posts.service';
import { Store } from '@ngrx/store';
import { filter, map, withLatestFrom } from 'rxjs/operators';
import { Component } from '@angular/core';

@Component({
  selector: 'app-post',
  template: `
    <div class="info">
      <dl>
        <dt>
          W przykładzie użyłem
        </dt>
        <dd>withLatestFrom operator - w celu połączenia strumieni</dd>
        <dd>NgRx/router-store - aktualny stan rutingu</dd>
      </dl>
      <pre>{{data$|async|json}}</pre>
    </div>
  `
})
export class PostComponent {

  data$ = this.store.select(selectRouterId)
    .pipe(
      filter(id => id),
      withLatestFrom(this.postsService.selectors$.entityMap$),
      map(([itemId, entities]) => {
        const { id, title } = entities[itemId];
        return { id, title };
      })
    )

  constructor(
    private postsService: PostsService,
    private store: Store<any>
  ) { }

}
