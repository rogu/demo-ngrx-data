import { PostsService } from '../services/posts.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-posts',
  template: `
    <div class="space-y-4">
      <dl class="info">
        <dt>
          W przykładzie użyłem
        </dt>
        <dd>Własny PostsService (EntityCollectionServiceBase) - możliwość przeciążania domyślnych metod oraz wstrzykiwania w wielu miejscach</dd>
        <dd>Angular Guard - zajmuje się dostarczeniem danych przed załadowaniem komponentu</dd>
      </dl>
      <div class="flex gap-4">
        <div>
          <div *ngFor="let item of data$|async|slice:0:15">
            {{item.title}}
            <button [routerLink]="item.id">more</button>
          </div>
        </div>
        <div>
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `
})
export class PostsComponent {

  data$: any = this.postsService.entities$;

  constructor(
    private postsService: PostsService
  ) { }

}
