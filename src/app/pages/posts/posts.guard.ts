import { PostsService } from './services/posts.service';
import { mapTo, filter, tap, first } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsGuard implements CanActivate {

  constructor(private postsService: PostsService) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.postsService.count$
      .pipe(
        tap((val) => !val && this.postsService.getAll()),
        filter((val) => !!val),
        mapTo(true),
        first()
      );
  }

}
