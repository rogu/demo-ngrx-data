import { PostsGuard } from './posts.guard';
import { PostsComponent } from './components/posts.component';
import { PostComponent } from './components/post.component';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [PostsComponent, PostComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: PostsComponent, canActivate: [PostsGuard], children: [
          { path: ':id', component: PostComponent }
        ]
      },
    ])
  ]
})
export class PostsModule { }
