import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { EntityCollectionServiceFactory, EntityCollectionService } from '@ngrx/data';

@Component({
  template: `
    <div class="space-y-4">
      <dl class="info">
        <dt>
          W przykładzie użyłem
        </dt>
        <dd>EntityCollectionServiceFactory - do utworzenia serwisu</dd>
        <dd>EntityMetadataMap - globalna konfiguracja Entity</dd>
      </dl>
      <div>
        <h3 class="font-bold">Users list</h3>
        <ul>
          <li *ngFor="let item of data$|async">
            {{item.name}}, {{item.email}}
          </li>
        </ul>
      </div>
    </div>
  `
})
export class UsersComponent implements OnInit {

  usersService: EntityCollectionService<any>;
  data$: Observable<any[]>;

  constructor(
    entityServiceFactory: EntityCollectionServiceFactory
  ) {
    this.usersService = entityServiceFactory.create('User');
  }

  ngOnInit(): void {
    this.data$ = this.usersService.entities$;
    this.usersService.getAll();
  }

}
