import { InitComponent } from './core/components/init/init.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '', component: InitComponent
  },
  {
    path: 'users', loadChildren: async () =>
      (await import('./pages/users/users.module')).UsersModule
  },
  {
    path: 'posts', loadChildren: async () =>
      (await import('./pages/posts/posts.module')).PostsModule
  },
  {
    path: 'items', loadChildren: async () =>
      (await import('./pages/items/items.module')).ItemsModule
  },
  {
    path: 'workers', loadChildren: async () =>
      (await import('./pages/workers/workers.module')).WorkersModule
  },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
