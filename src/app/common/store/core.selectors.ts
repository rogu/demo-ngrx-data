import { createFeatureSelector, createSelector } from '@ngrx/store';
import { EntityState } from "@ngrx/entity";
import { getRouterSelectors } from '@ngrx/router-store';

const { selectRouteParams, selectUrl } = getRouterSelectors();

export const selectRouterUrl = createSelector(
  selectUrl,
  (url) => url
)

export const selectRouterId = createSelector(
  selectRouteParams,
  ({ id }) => id
)

export const selectLoading = createSelector(
  createFeatureSelector('entityCache'),
  (states: EntityState<any>) => !!Object.values(states).find((state) => state.loading)
)
