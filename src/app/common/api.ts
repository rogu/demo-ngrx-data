export class Api {
    static DATA_URL = "https://api.debugger.pl/";
    static AUTH_URL = "https://auth.debugger.pl/";

    // Data end-points
    static DATA_ITEMS = Api.DATA_URL + "items";
    static DATA_WORKERS = Api.DATA_URL + "workers";

    // Auth end-points
    static AUTH_LOGIN = Api.AUTH_URL + "login";
    static AUTH_LOGOUT = Api.AUTH_URL + "logout";
    static AUTH_IS_LOGGED = Api.AUTH_URL + "is-logged";
}
