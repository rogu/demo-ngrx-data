import { CoreService } from './core/services/core.service';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { Injectable } from "@angular/core";
import { tap, catchError, map } from 'rxjs/operators';

@Injectable()
export class XHRInterceptor implements HttpInterceptor {

    constructor(private coreService: CoreService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = localStorage.getItem('token');
        const opt = token ? { setHeaders: { 'authorization': token } } : {};
        const reqClone = req.clone({ ...opt });
        return next
            .handle(reqClone)
            .pipe(
                tap((evt: HttpEvent<any>) => {
                    if (evt instanceof HttpResponse) {
                        //console.log('---> status:', evt.status);
                    }
                }),
                catchError(({ status, error }) => {
                    status === 401 && this.coreService.logout();
                    alert(error.error || 'any error');
                    return throwError(() => error);
                })
            )
    }
}
