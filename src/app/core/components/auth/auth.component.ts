import { map } from 'rxjs/operators';
import { CoreService } from './../../services/core.service';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-auth',
  template: `
    <div>
      <ng-template #form>
      <form class="flex gap-2" (ngSubmit)="login(f.value)" #f="ngForm">
        <input name='username' ngModel='admin@localhost'>
        <input name='password' ngModel='Admin1'>
        <button class="btn">log in</button>
      </form>
      </ng-template>
      <div *ngIf="token$|async;else form">
        <button (click)='logout()'>logout</button>
      </div>
    </div>
  `
})
export class AuthComponent {

  token$: Observable<string> = this.coreService.getByKey('auth').pipe(map((state) => state.accessToken));

  constructor(
    private coreService: CoreService
  ) { }

  login(val) {
    this.coreService.login(val).subscribe();
  }

  logout() {
    this.coreService.logout();
  }

}
