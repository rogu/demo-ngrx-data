import { Observable } from 'rxjs';
import { selectRouterUrl, selectLoading } from '../../../common/store/core.selectors';
import { Store, select } from '@ngrx/store';
import { Component } from '@angular/core';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  routerState$: Observable<string> = this.store.pipe(select(selectRouterUrl));
  loading$: Observable<boolean> = this.store.pipe(select(selectLoading), delay(0));

  constructor(private store: Store) { }

}
