export interface ResponseAuth {
    data:any;
    message: string;
    error: string;
    accessToken: string;
    refreshToken: string;
}
