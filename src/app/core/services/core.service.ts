import { ResponseAuth } from './../core.model';
import { map } from 'rxjs/operators';
import { EntityCollectionServiceBase, EntityCollectionServiceElementsFactory } from '@ngrx/data';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Api } from 'src/app/common/api';

@Injectable({ providedIn: 'root' })
export class CoreService extends EntityCollectionServiceBase<any> {

    constructor(
        private http: HttpClient,
        serviceElementsFactory: EntityCollectionServiceElementsFactory
    ) {
        super('Core', serviceElementsFactory);
        const accessToken = localStorage.getItem('token');
        this.addOneToCache({ id: 'auth', accessToken });
    }

    login(creds) {
        return this.http.post(Api.AUTH_LOGIN, creds)
            .pipe(map(({ data: { accessToken } }: ResponseAuth) => {
                this.upsertOneInCache({ id: 'auth', accessToken });
                localStorage.setItem('token', accessToken);
                return accessToken;
            }))
    }

    logout() {
        localStorage.removeItem('token');
        this.updateOneInCache({ id: 'auth', accessToken: null });
    }

    getByKey(key) {
        return this.entityMap$.pipe(map((entities) => entities[key]));
    }

}
