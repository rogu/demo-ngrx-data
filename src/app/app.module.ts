
import { AppComponent } from './core/components/main/app.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { EntityDataModule, DefaultDataServiceConfig } from '@ngrx/data';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { AuthComponent } from './core/components/auth/auth.component';
import { FormsModule } from "@angular/forms";
import { StoreRouterConnectingModule, routerReducer } from '@ngrx/router-store';
import { InitComponent } from './core/components/init/init.component';
import { entityConfig, defaultDataServiceConfig } from './entity-metadata';
import { XHRInterceptor } from './xhr-interceptor';

@NgModule({
  declarations: [AppComponent, AuthComponent, InitComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot({
      router: routerReducer
    }),
    EffectsModule.forRoot([]),
    EntityDataModule.forRoot(entityConfig),
    HttpClientModule,
    FormsModule,
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    StoreRouterConnectingModule.forRoot()
  ],
  providers: [
    { provide: DefaultDataServiceConfig, useValue: defaultDataServiceConfig },
    { provide: HTTP_INTERCEPTORS, useClass: XHRInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
